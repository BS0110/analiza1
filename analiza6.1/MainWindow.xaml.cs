﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ForSchoolWPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public double firstnumber;
        public double secondnumber;
        public string lastoperation;

        public MainWindow()
        {
            firstnumber = secondnumber = 0;
            lastoperation = "";
            InitializeComponent();
        }

        private void Plus_Click(object sender, RoutedEventArgs e)
        {
            Equals_Click(null, null);

            if (Numpad.Text == "")
            {
                Numpad.Text = "0";
            }

            if (firstnumber == 0)
            {
                if (!double.TryParse(Numpad.Text, out firstnumber))
                    Numpad.Text = "Err";
                else
                {
                    lastoperation = "+";
                }
            }
            else
            {
                if (!double.TryParse(Numpad.Text, out secondnumber))
                    Numpad.Text = "Err";
                else
                {
                    firstnumber += secondnumber;
                    secondnumber = 0;
                    Numpad.Text = firstnumber.ToString();
                    lastoperation = "+";
                }
            }
        }

        private void Equals_Click(object sender, RoutedEventArgs e)
        {
            if (Numpad.Text == "")
            {
                Numpad.Text = "0";
            }
            if (firstnumber == 0)
            {
                if (!double.TryParse(Numpad.Text, out firstnumber))
                    Numpad.Text = "Err";
                else
                {
                    Numpad.Text = "";
                }
            }
            else
            {
                if (!double.TryParse(Numpad.Text, out secondnumber))
                    Numpad.Text = "Err";
                else
                {
                    Numpad.Text = "";
                }
            }

            if (firstnumber == 0)
            {
                Numpad.Text = "0";
            }
            else
            {
                switch (lastoperation)
                {
                    case "+":
                        Numpad.Text = (firstnumber + secondnumber).ToString();
                        firstnumber = 0;
                        secondnumber = 0;
                        break;
                    case "-":
                        Numpad.Text = (firstnumber - secondnumber).ToString();
                        firstnumber = 0;
                        secondnumber = 0;
                        break;
                    case "*":
                        Numpad.Text = (firstnumber * secondnumber).ToString();
                        firstnumber = 0;
                        secondnumber = 0;
                        break;
                    case "/":
                        Numpad.Text = (firstnumber / secondnumber).ToString();
                        firstnumber = 0;
                        secondnumber = 0;
                        break;
                    default:
                        break;

                }
            }
        }

        private void Minus_Click(object sender, RoutedEventArgs e)
        {
            Equals_Click(null, null);

            if (Numpad.Text == "")
            {
                Numpad.Text = "0";
            }

            if (firstnumber == 0)
            {
                if (!double.TryParse(Numpad.Text, out firstnumber))
                    Numpad.Text = "Err";
                else
                {
                    lastoperation = "-";
                }
            }
            else
            {
                if (!double.TryParse(Numpad.Text, out secondnumber))
                    Numpad.Text = "Err";
                else
                {
                    firstnumber -= secondnumber;
                    secondnumber = 0;
                    Numpad.Text = firstnumber.ToString();
                    lastoperation = "-";
                }
            }
        }

        private void Multiply_Click(object sender, RoutedEventArgs e)
        {
            Equals_Click(null, null);

            if (Numpad.Text == "")
            {
                Numpad.Text = "0";
            }

            if (firstnumber == 0)
            {
                if (!double.TryParse(Numpad.Text, out firstnumber))
                    Numpad.Text = "Err";
                else
                {
                    lastoperation = "*";
                }
            }
            else
            {
                if (!double.TryParse(Numpad.Text, out secondnumber))
                    Numpad.Text = "Err";
                else
                {
                    firstnumber *= secondnumber;
                    secondnumber = 0;
                    Numpad.Text = firstnumber.ToString();
                    lastoperation = "*";
                }
            }
        }

        private void Divide_Click(object sender, RoutedEventArgs e)
        {
            Equals_Click(null, null);

            if (Numpad.Text == "")
            {
                Numpad.Text = "0";
            }

            if (firstnumber == 0)
            {
                if (!double.TryParse(Numpad.Text, out firstnumber))
                    Numpad.Text = "Err";
                else
                {
                    lastoperation = "/";
                }
            }
            else
            {
                if (!double.TryParse(Numpad.Text, out secondnumber))
                    Numpad.Text = "Err";
                else
                {
                    firstnumber /= secondnumber;
                    secondnumber = 0;
                    Numpad.Text = firstnumber.ToString();
                    lastoperation = "/";
                }
            }
        }

        private void Sinus_Click(object sender, RoutedEventArgs e)
        {
            if (lastoperation != "")
            {
                Equals_Click(null, null);
                firstnumber = secondnumber = 0;
            }
            if (!double.TryParse(Numpad.Text, out firstnumber))
                Numpad.Text = "Err";
            firstnumber = Math.Sin(firstnumber);
            lastoperation = "";
            Numpad.Text = firstnumber.ToString();
        }

        private void Cosinus_Click(object sender, RoutedEventArgs e)
        {
            if (lastoperation != "")
            {
                Equals_Click(null, null);
                firstnumber = secondnumber = 0;
            }
            if (!double.TryParse(Numpad.Text, out firstnumber))
                Numpad.Text = "Err";
            firstnumber = Math.Cos(firstnumber);
            lastoperation = "";
            Numpad.Text = firstnumber.ToString();
        }

        private void SquareRoot_Click(object sender, RoutedEventArgs e)
        {
            if (lastoperation != "")
            {
                Equals_Click(null, null);
                firstnumber = secondnumber = 0;
            }
            if (!double.TryParse(Numpad.Text, out firstnumber))
                Numpad.Text = "Err";
            firstnumber = Math.Sqrt(firstnumber);
            lastoperation = "";
            Numpad.Text = firstnumber.ToString();
        }

        private void Power_Click(object sender, RoutedEventArgs e)
        {
            if (lastoperation != "")
            {
                Equals_Click(null, null);
                firstnumber = secondnumber = 0;
            }
            if (!double.TryParse(Numpad.Text, out firstnumber))
                Numpad.Text = "Err";
            firstnumber = Math.Pow(firstnumber, 2);
            lastoperation = "";
            Numpad.Text = firstnumber.ToString();

        }

        private void Logarithm_Click(object sender, RoutedEventArgs e)
        {
            if (lastoperation != "")
            {
                Equals_Click(null, null);
                firstnumber = secondnumber = 0;
            }
            if (!double.TryParse(Numpad.Text, out firstnumber))
                Numpad.Text = "Err";
            firstnumber = Math.Log(firstnumber);
            lastoperation = "";
            Numpad.Text = firstnumber.ToString();
        }
    }
}